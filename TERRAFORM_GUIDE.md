# TERRAFORM 
## What is Terraform?

Before starting with Terraform let us understand what is IaC. Infrastructure as code (IaC) means to manage your IT infrastructure using configuration files.
<p>
Terraform is an <b>infrastructure as code (IaC)</b> tool created by HarshiCorp that lets you define both cloud and on-prem resources in human-readable configuration files that you can version, reuse, and share :recycle:.
<p>
The infrastructure Terraform manages can be hosted on public clouds like Amazon Web Services, Microsoft Azure, and Google Cloud Platform, or on-prem in private clouds such as VMWare vSphere, OpenStack, or CloudStack.
<p>
Terraform users define infrastructure configurations by using a JSON-like configuration language called <b>HCL (HashiCorp Configuration Language)</b> :page_facing_up:. HCL's simple syntax makes it easy for DevOps teams to provision and re-provision infrastructure across multiple cloud and on-premises data centers.
<p>
HashiCorp offers a commercial version of Terraform called Terraform Enterprise. According to the HashiCorp website, the commercial version includes enterprise features on top of open source Terraform and includes a framework called Sentinel that can implement policy as code.

## How Terraform Works?
<p>
Terraform creates and manages resources on cloud platforms and other services through their <b>application programming interfaces (APIs)</b>. Providers enable Terraform to work with virtually any platform or service with an accessible API.
<p>
![1.png](./1.png)

<p>
Terraform has two important components: <b>Terraform Core</b> and <b>Terraform Plugins</b>.
<p>
<h4>Terraform Core</h4>
Terraform Core is the entrypoint for anyone using Terraform.
Core is composed of binaries written in the Go programming language.
<p>
Terraform core uses two input sources to do its job.
The first input source is a Terraform configuration file. .tf
The second input source is a state where terraform keeps the up-to-date state of how the current set up of the infrastructure looks like. i.
It compares the state, what is the current state, and what is the configuration that you desire in the end result. It figures out what needs to be created, what needs to be updated, what needs to be deleted to create and provision the infrastructure
<p>

<h4>Terraform Plugins :electric_plug:</h4>
<p>
Terraform Plugins are responsible for defining resources for specific services. 
Provisioner plugins are used to execute commands for a designated resource.
Terraform has over a hundred providers for different technologies, and each provider then gives terraform user access to its resources.

## Terraform Core Concepts 
> <b>Variables:</b> they are used as input-variables, it is key-value pair used by Terraform modules to allow customization.<br>
> <b>Provider:</b> It is a plugin used to interact with APIs of service and access its related resources.<br>
> <b>State:</b> It consists of cached information about the infrastructure managed by Terraform and the related configurations. Available backend for storing state are: local, remote, s3, azureRM, k8s, etcd, etc.<br>
> __Resources:__ Are a block , which are used in configuring and managing the infrastructure.
Output Values: These are return values of a terraform module that can be used by other configurations.<br>
> __Plan:__ It is one of the stages where it determines what needs to be created, updated, or destroyed to move from real/current state of the infrastructure to the desired state.<br>
> __Apply__: It is one of the stages where it applies the changes real/current state of the infrastructure in order to move to the desired state.<br>
> __Module__: It is a folder with Terraform templates where all the configurations are defined, it is best practice to follow this structure.
A module is a container for multiple resources that are used together.<br>
Every Terraform configuration has at least one module, known as its root module, which consists of the resources defined in the .tf files in the main working directory.<br>
A module can call other modules, which lets you include the child module's resources into the configuration in a concise way. Modules can also be called multiple times, either within the same configuration or in separate configurations, allowing resource configurations to be packaged and re-used.

## Install Terraform CLI
<p>
<b> For Ubuntu/Debian </b>

   ```bash
   - wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
   - echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
   - sudo apt update && sudo apt install terraform
   ```

<b> For CentOS/RHEL </b>

   ```bash
   - sudo yum install -y yum-utils
   - sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
   - sudo yum -y install terraform
   ```

<b> For Amazon Linux </b>

   ```bash
   - sudo yum install -y yum-utils
   - sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
   - sudo yum -y install terraform
   ```

<b> For macOS </b>
   ```bash
   - brew tap hashicorp/tap
   - brew install hashicorp/tap/terraform
   ```
## About Terraform Language
<p>
The main purpose of the Terraform language is declaring resources, which represent infrastructure objects. All other language features exist only to make the definition of resources more flexible and convenient.

A Terraform configuration is a complete document in the Terraform language that tells Terraform how to manage a given collection of infrastructure. A configuration can consist of multiple files and directories.

The syntax of the Terraform language consists of only a few basic elements:

```yaml
resource "aws_vpc" "main" {
  cidr_block = var.base_cidr_block
}

<BLOCK TYPE> "<BLOCK LABEL>" "<BLOCK LABEL>" {
  # Block body
  <IDENTIFIER> = <EXPRESSION> # Argument
}
```

* _Blocks_ are containers for other content and usually represent the configuration of some kind of object, like a resource. Blocks have a block type, can have zero or more labels, and have a body that contains any number of arguments and nested blocks. Most of Terraform's features are controlled by top-level blocks in a configuration file.
* _Arguments_ assign a value to a name. They appear within blocks.
* _Expressions_ represent a value, either literally or by referencing and combining other values. They appear as values for arguments, or within other expressions.


## Terraform Lifecycle
<p>
Terraform lifecycle consists of – <b>init</b>, <b>plan</b>, <b>apply</b>, and <b>destroy</b> phases.
<p>
<b>Terraform Init</b>
> The terraform init command is used to <b>initialize</b> a working directory containing Terraform configuration files. This is the first command that should be run after writing a new Terraform configuration or cloning an existing one from version control. It is safe to run this command multiple times.<br>

<b>Usage:</b><br>

 ```bash 
  - terraform init [options]
 ```

<b>Terraform Plan</b>

> The terraform plan command creates an <b>execution plan</b>, which lets you preview the changes that Terraform plans to make to your infrastructure. By default, when Terraform creates a plan it:<br>
> * Reads the current state of any already-existing remote objects to make sure that the Terraform state is up-to-date.<br>
> * Compares the current configuration to the prior state and noting any differences.<br>
> * Proposes a set of change actions that should, if applied, make the remote objects match the configuration.
> 
> The plan command alone will not actually carry out the proposed changes, and so you can use this command to check whether the proposed changes match what you expected before you apply the changes

<b>Usage:</b><br>

 ```bash 
  - terraform plan [options]
 ```

<b>Terraform Apply</b>

> The terraform apply command performs a plan just like terraform plan does, but then <b>actually carries out the planned changes</b> to each resource using the relevant infrastructure provider's API. It asks for confirmation from the user before making any changes, unless it was explicitly told to skip approval.<br>
> 
> By default, terraform apply performs a fresh plan right before applying changes, and displays the plan to the user when asking for confirmation. <br>
> Another way to use terraform apply is to pass it the filename of a saved plan file you created earlier with <b>terraform plan -out=...</b>, in which case Terraform will apply the changes in the plan without any confirmation prompt.

<b>Usage:</b><br>

 ```bash 
  - terraform apply [options]
 ```

<b>Terraform Destroy</b>

> The terraform destroy command is a convenient way to <b>destroy all remote objects</b> managed by a particular Terraform configuration.<br>
> The terraform destroy command destroys all of the resources being managed by the current working directory and workspace, using state data to determine which real world objects correspond to managed resources. Like terraform apply, it asks for confirmation before proceeding.
> A destroy behaves exactly like deleting every resource from the configuration and then running an apply, except that it doesn't require editing the configuration.


<b>Usage:</b><br>

 ```bash 
  - terraform destroy [options]
 ```

